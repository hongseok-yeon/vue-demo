import Vue from 'vue'
import Vuex from 'vuex'

const debug = process.env.NODE_ENV !== 'production'
Vue.use(Vuex)

const state = {
  count: 0,
  message: null
}

const getters = {
  getCountMessage (state, getters, rootState, rootGetters) {
    return `count is ${state.count}`
  }
}

const mutations = {
  increaseCount (state, payload, invalidParam) {
    console.log(state, payload, invalidParam)

    if (payload && !isNaN(Number(payload))) {
      state.count += payload
    } else {
      state.count++
    }
  },
  setMessage (state, payload) {
    state.message = payload
  }
}

const actions = {
  getCountAsync (context, payload, invalidParam) {
    console.log(context, payload, invalidParam)

    return new Promise((resolve) => {
      window.setTimeout(() => {
        context.commit('increaseCount', payload, invalidParam)
        resolve(context.state.count)
      }, 1000)
    })
  }
}

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  strict: debug
})
