export default {
  data () {
    return {
      mixinMessage: 'mixin message'
    }
  },
  created () {
    console.log('sample mixin created')
  },
  methods: {
    changeMixinMessage () {
      this.mixinMessage = 'new mixin message'
    }
  }
}
