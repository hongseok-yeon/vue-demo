import 'es6-promise/auto'
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import VueMarkdown from 'vue-markdown'

Vue.config.productionTip = false

// register global component
Vue.component('vue-markdown', VueMarkdown)

Vue.filter('numberFormat', (value) => {
  value = Number(value)

  if (isNaN(value)) {
    return value
  }
  return value.toLocaleString()
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
