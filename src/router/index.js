import Vue from 'vue'
import Router from 'vue-router'

import BaseBasic from '@/components/basic/BaseBasic'
import DataReactivity from '@/components/basic/data-reactivity'
import DataBinding from '@/components/basic/data-binding'
import StyleClass from '@/components/basic/style-class'
import SetData from '@/components/basic/set-data'
import ListRendering from '@/components/basic/list-rendering'
import DataWatcher from '@/components/basic/data-watcher'
import LifeCycle from '@/components/basic/life-cycle'
import NextTick from '@/components/basic/next-tick'
import AppMixin from '@/components/basic/app-mixin'
import AppFilter from '@/components/basic/app-filter'

import BaseComponent from '@/components/component/BaseComponent'
import PropsEvents from '@/components/component/props-events'
import ContentSlot from '@/components/component/content-slot'
import ScopedSlot from '@/components/component/scoped-slot'
import DynamicView from '@/components/component/dynamic-view'

import BaseStore from '@/components/store/BaseStore'
import StoreBasic from '@/components/store/store-basic'
import TwoWayBinding from '@/components/store/two-way-binding'

import BaseRouter from '@/components/router/BaseRouter'
import RouteParams from '@/components/router/route-params'
import DataFetching from '@/components/router/data-fetching'
import RouteMeta from '@/components/router/children/RouteMeta'
import RouteInfo from '@/components/router/children/RouteInfo'

import UiDemo from '@/components/ui/UiDemo'
import UiDemoIframe from '@/components/ui/UiDemoIframe'
import TabDemo from '@/components/ui/tab-demo'

import AppHome from '@/components/etc/app-home'
import RouterNotFound from '@/components/etc/router-not-found'
import CustomError from '@/components/etc/custom-error'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: AppHome,
      alias: ['/home', '/main']
    },
    {
      path: '/error',
      name: 'CustomError',
      component: CustomError
    },
    {
      path: '/basic',
      name: 'Basic',
      component: BaseBasic,
      redirect: { name: 'Reactivity' },
      title: 'Basic',
      children: [
        {
          path: 'data-reactivity',
          name: 'DataReactivity',
          component: DataReactivity,
          title: 'Data Reactivity'
        },
        {
          path: 'data-binding',
          name: 'DataBinding',
          component: DataBinding,
          title: 'Data Binding'
        },
        {
          path: 'style-class',
          name: 'StyleClass',
          component: StyleClass,
          title: 'Style, class'
        },
        {
          path: 'set-data',
          name: 'SetData',
          component: SetData,
          title: 'Set data'
        },
        {
          path: 'list-rendering',
          name: 'ListRendering',
          component: ListRendering,
          title: 'List rendering'
        },
        {
          path: 'data-watcher',
          name: 'DataWatcher',
          component: DataWatcher,
          title: 'Data Watcher'
        },
        {
          path: 'life-cycle',
          name: 'LifeCycle',
          component: LifeCycle,
          title: 'Life cycle'
        },
        {
          path: 'next-tick',
          name: 'NextTick',
          component: NextTick,
          title: 'Next tick'
        },
        {
          path: 'app-mixin',
          name: 'AppMixin',
          component: AppMixin,
          title: 'Mixin'
        },
        {
          path: 'app-filter',
          name: 'AppFilter',
          component: AppFilter,
          title: 'Filter'
        }
      ]
    },
    {
      path: '/component',
      name: 'Component',
      component: BaseComponent,
      title: 'Component',
      children: [
        {
          path: 'props-events',
          name: 'PropsEvents',
          component: PropsEvents,
          title: 'Props & events'
        },
        {
          path: 'content-slot',
          name: 'ContentSlot',
          component: ContentSlot,
          title: 'Basic slot'
        },
        {
          path: 'scoped-slot',
          name: 'ScopedSlot',
          component: ScopedSlot,
          title: 'Scoped slot'
        },
        {
          path: 'dynamic-view',
          name: 'DynamicView',
          component: DynamicView,
          title: 'Dynamic view'
        }
      ]
    },
    {
      path: '/store',
      name: 'Store',
      redirect: { name: 'StoreBasic' },
      component: BaseStore,
      title: 'Store',
      children: [
        {
          path: 'basic',
          name: 'StoreBasic',
          component: StoreBasic,
          title: 'Basic'
        },
        {
          path: 'two-way-binding',
          name: 'TwoWayBinding',
          component: TwoWayBinding,
          title: 'Two-way binding'
        }
      ]
    },
    {
      path: '/router',
      name: 'Router',
      component: BaseRouter,
      title: 'Router',
      children: [
        {
          path: 'params/:id(\\d+)?/:message?',
          name: 'RouteParams',
          component: RouteParams,
          props: true,
          title: 'Dynamic Routing',
          customParams: {
            id: 5,
            message: 'Hello'
          }
        },
        {
          path: 'data-fetching',
          components: {
            default: DataFetching,
            info: RouteInfo
          },
          title: 'Data fetching',
          children: [
            {
              path: '',
              name: 'DataFetching',
              title: 'Route data fetching'
            },
            {
              path: 'with-meta',
              name: 'DataFetchingWithMeta',
              components: {
                meta: RouteMeta
              },
              title: 'Route data fetching with meta'
            }
          ]
        }
      ]
    },
    {
      path: '/ui',
      name: 'UiDemo',
      component: UiDemo,
      title: 'UI demo',
      children: [
        {
          path: 'tab-demo',
          name: 'TabDemo',
          component: TabDemo,
          title: 'Tab demo'
        },
        {
          path: 'iframe/to-do',
          name: 'ToDoDemo',
          component: UiDemoIframe,
          title: 'To-do demo',
          props: {
            demoType: 'to-do'
          }
        },
        {
          path: 'iframe/modal',
          name: 'ModalDemo',
          component: UiDemoIframe,
          title: 'Modal demo',
          props: {
            demoType: 'modal'
          }
        },
        {
          path: 'iframe/graph',
          name: 'GraphDemo',
          component: UiDemoIframe,
          title: 'Graph demo',
          props: {
            demoType: 'graph'
          }
        }
      ]
    },
    {
      path: '/404',
      name: 'RouterNotFound',
      component: RouterNotFound
    },
    {
      path: '*',
      component: RouterNotFound
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.onError((error) => {
  console.log('router error')
  console.error(error)
  router.push({
    name: 'CustomError',
    params: { message: 'route.onError handling' }
  })
})

export default router
